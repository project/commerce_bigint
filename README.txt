Commerce BIGINT
Copyright 2016 Chris Caldwell <chrisolof@gmail.com>


Description
-----------
Allows Drupal Commerce 1.x to store larger price amounts.

Drupal Commerce 1.x uses standard integer storage for price amounts.  While
this works for the majority of installs, some sites need the ability to store
larger price amounts.

Commerce BIGINT allows for storage of larger prices by increasing the size of
the integer fields used to store those amounts.  Different storage backends
and currency decimal precision will influence the theoretical maxiumum amount
that can be stored, but take a common setup for example:

Storable Commerce Price Range for MySQL Storage with USD currency:
Without Commerce BIGINT:  $-21,474,836.48 - $21,474,836.47
With Commerce BIGINT:  $-92,233,720,368,547,758.08 - $92,233,720,368,547,758.07


Installation
------------
1. Backup your site's database and files in case something goes wrong or you
wish to roll-back to standard INT storage.

2. Download and place this module into your site's modules folder
-or-
drush dl commerce_bigint

3. Patch Drupal Commerce with the included commerce-price-storage-bigint.patch
file.

See https://www.drupal.org/patch/apply

This patch is necessary because there is currently no hook to alter the
commerce_price field schema, and it is unlikely that Drupal Commerce 1.x will
ever adopt BIGINT storage as the default price field storage.

4. Enable the "Commerce BIGINT" module, which can be found under
"Commerce (contrib)" on the admin/modules page.
-or-
drush en commerce_bigint

Note:  If you have a large amount of price data already stored in your database,
installing the module via Drush is *strongly* recommended.  The conversion
process from INT to BIGINT can take some time for large datasets.  To
eliminate the risk of timing out mid-conversion, use Drush.
drush en commerce_bigint


Commerce 2.x
------------
Commerce 2.x utilizes a completely different storage strategy for price amounts
and is therefore not nearly as limited in terms of storable price range.
As a result, Commerce BIGINT only targets Drupal Commerce 7.x-1.x.


Uninstallation
--------------
If you uninstall this module any fields that were converted to BIGINT storage
will remain BIGINT.  This is by design to avoid truncating existing stored data.
Post-uninstallation, any newly-created price fields will be created using
standard INT storage.

To uninstall:
1. Backup your site's database and files in case something goes wrong or you
wish to roll-back to BIGINT storage.
2. Disable and uninstall the "Commerce BIGINT" module, which can be found under
"Commerce (contrib)" on the admin/modules page.
3. Replace your patched copy of Drupal Commerce with an unpatched one from
https://www.drupal.org/project/commerce


Bugs, Features, & Patches
-------------------------
If you wish to report bugs, add feature requests, or submit patches, you can do
so on the project page on Drupal.org.
http://drupal.org/project/commerce_bigint


Acknowledgements 
----------------
Much of the development of Commerce BIGINT was sponsored by CivicSolar
(https://www.civicsolar.com/).


Author
------
Chris Caldwell (https://www.drupal.org/u/chrisolof) <chrisolof@gmail.com>
